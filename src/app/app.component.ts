import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { resolve } from 'url';
import { reject } from 'q';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
 genders = ['male', 'female'];
 signupForm: FormGroup;
 control;
 forbiddenUserName = ['Chris', 'Anna'];

ngOnInit() {
  this.signupForm = new FormGroup({
    userData: new FormGroup({
      username: new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)
      ]),
      email : new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmail)
     }),
    gender: new FormControl('female'),
    hobbies: new FormArray([])
 });
//   this.signupForm.valueChanges.subscribe(
//     (value) => console.log(value)
//  );
  this.signupForm.statusChanges.subscribe(
     (status) => console.log(status)
);
  this.signupForm.setValue({
    userData: {
      username: 'Max',
      email: 'max@test.com'
    },
    gender: 'male',
    hobbies: []
  });
  this.signupForm.patchValue({
    userData: {
      username: 'Chris'
    }
  });
}
onSubmit() {
  console.log(this.signupForm);
  this.signupForm.reset();
}
onAddHobby() {
  this.control = new FormControl(null, Validators.required);
  (this.signupForm.get('hobbies') as FormArray).push(this.control);
}
forbiddenNames(control: FormControl): {[s: string]: boolean} {
  if (this.forbiddenUserName.indexOf(control.value) !== -1) {
    return { nameIsForbidden: true };

  }
  return null;
}
forbiddenEmail(control: FormControl): Promise<any> | Observable<any> {
  const promise = new Promise<any>((resolve, reject) => {
    setTimeout(() => {
      if (control.value === 'test@test.com') {
          resolve ({ emailIsForbidden: true });
      } else {
        resolve(null);
      }
     },  1500);
  });

  return promise;
  }
}
